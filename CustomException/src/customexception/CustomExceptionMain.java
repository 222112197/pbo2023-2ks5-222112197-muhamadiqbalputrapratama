/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package customexception;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Main class yang menerapkan InvalidInputException
 */
public class CustomExceptionMain {
    // Method ini akan meng-throws InvalidInputException dengan sebuah pesan
    public static void justThrowException() throws InvalidInputException{
        throw new InvalidInputException("Input harus berupa string");
    }
    
    // Main method yang akan meng-catches eror yang di throws oleh methor justThrowException
    public static void main(String[] args){
        try{
            justThrowException();
        }catch(InvalidInputException e){
            e.printStackTrace();
        }
    }
}
   
