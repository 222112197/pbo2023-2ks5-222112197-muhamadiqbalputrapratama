/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package customexception;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Class Invalid Input Exception yang akan digunakan untuk mengecek 
 *            inputan user
 */

public class InvalidInputException extends Exception{
    public InvalidInputException(String statement){
        super(statement);
    }
}

