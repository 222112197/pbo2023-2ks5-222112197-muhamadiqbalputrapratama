/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Box;

/**
 *
 * @author hp
 */
public class BoxTest {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //UNTUK CLASS BOX
        /*Box boxes[] = new Box[5];
        for(int i=0;i<boxes.length;i++){
            boxes[i] = new Box();
        }
        boxes[0].set(10);
        boxes[1].set("Polstat STIS");
        Integer someInteger =(Integer) boxes[0].get();
        String someString =(String) boxes[1].get();
        System.out.println(someInteger);
        System.out.println(someString); */
        //UNTUK CLASS BOX2
        //Old nonGeneric style
        Box2 stringBox = new Box2();
        stringBox.set("Polstat STIS");
        String someString = (String) stringBox.get();
        System.out.println(someString);
        //new generic style
        Box2<String> box1 = new Box2<>();
        box1.set("Jakarta");
        String string1 = box1.get();
        System.out.println(string1);
        //can also store another type
        Box2<Integer> box2 = new Box2<>();
        box2.set(100);
        int a = box2.get(); //no casting
        System.out.println(a);
        
}
}