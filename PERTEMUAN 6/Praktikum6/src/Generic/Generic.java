/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Generic;

import Box.Box2;

/**
 *
 * @author hp
 */
public class Generic {
    public static void main(String[] args){
        //Java 7 style
        /*
        Pair<String, Integer> p1 = new OrderedPair<String, Integer> ("Even", 8);
        Pair<String, String> p2 = new OrderedPair<String, String> ("hello", "world");
        */
        
        //Java 8 and later
        OrderedPair<String, Integer> p1 = new OrderedPair<> ("Even", 8);
        OrderedPair<String, String> p2 = new OrderedPair<> ("hello", "world");
    
        //Parameterized type
        OrderedPair<String, Box2<Integer>> p = new OrderedPair<>("primes", new Box2<>());
    }
}
