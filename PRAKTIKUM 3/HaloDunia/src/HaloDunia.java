/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

package halodunia;
/*
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class ini akan menampilkan output "Halo Dunia" tampa argumen
 */

public class HaloDunia {
    public static void main(String[] args) {
        System.out.println("Halo, Dunia!");
    }
    
}
