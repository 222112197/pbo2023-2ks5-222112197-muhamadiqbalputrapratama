/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package halodunia;
/*
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class ini akan menampilkan output "Halo Dunia" dengan argumen berupa NIM dan nama
 */

public class HaloDunia2 {
    public static void main(String[] args) {
	System.out.println("Halo, Dunia! dengan argumen:");
	for(int i=0;i<args.length;i++)
		System.out.println(args[i]);
	}
}
