/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;
/*
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class Mahasiswa3, berisi nim dan nama dengan tambahan method untuk mengubah nim, nama, dan menampilkan data tersebut
 *            Class ini digunakan untuk instansiasi objek di class MahasiswaMain4
 */

public class Mahasiswa3 {
    int nim;
    String nama;
    
    // Untuk mengubah nilai NIM dan nama mahasiswa
    void tambahData(int vnim, String vnama){
        nim = vnim;
        nama = vnama;
    }
    
    // Untuk menampilkan data NIM dan nama mahasiswa
    void tampilkanInfo(){
        System.out.println(nim + " " + nama);
    }
}
