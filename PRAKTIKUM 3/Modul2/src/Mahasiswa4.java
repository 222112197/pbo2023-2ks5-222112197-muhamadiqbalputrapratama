/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;
/*
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class Mahasiswa4, menggunakan konstruktor dengan parameter nim dan nama, 
 *            Disertai method tampilkanInfo untuk menampilkan data
 *            Class ini digunakan untuk instansiasi objek di class MahasiswaMain5
 */

public class Mahasiswa4 {
    int nim;
    String nama;
    
    // Konstruktor
    Mahasiswa4(int vnim, String vnama){
        nim = vnim;
        nama = vnama;
    }
    
    // Untuk menampilkan data NIM dan nama mahasiswa
    void tampilkanInfo(){
        System.out.println(nim + " " + nama);
    }
}
