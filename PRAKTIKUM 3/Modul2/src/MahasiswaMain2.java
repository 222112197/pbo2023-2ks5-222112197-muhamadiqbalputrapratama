/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;
/*
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class MahasiswaMain2, menampilkan output NIM dan nama mahasiswa 
 *            disertai pembentukan objek orang "Lutfi"
 */

public class MahasiswaMain2 {
    public static void main(String args[]){
        Mahasiswa2 s1 = new Mahasiswa2();
        s1.nim = 123456;
        s1.nama = "Lutfi";
        System.out.println(s1.nim);
        System.out.println(s1.nama);
}
}
