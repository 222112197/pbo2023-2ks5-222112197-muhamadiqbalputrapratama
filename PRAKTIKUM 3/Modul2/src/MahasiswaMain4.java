/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;
/*
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class MahasiswaMain4, menggunakan method tambahData untuk 
 *            menambahkan nilai NIM dan nama mahasiswa, kemudian menampilkannya 
 *            dengan method tampilkanInfo 
 */

public class MahasiswaMain4 {
    public static void main(String args[]){
        Mahasiswa3 s1 = new Mahasiswa3();
        Mahasiswa3 s2 = new Mahasiswa3();
        s1.tambahData(123456, "Lutfi");
        s2.tambahData(123457, "Rahma");
        s1.tampilkanInfo();
        s2.tampilkanInfo();
    }
}
