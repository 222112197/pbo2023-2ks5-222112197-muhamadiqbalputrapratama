/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;
/*
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class MahasiswaMain5, menginisiasi objek Lutfi dan Rahma, 
 *            kemudian menampilkan datanya dengan method tampilkanInfo 
*/

public class MahasiswaMain5 {
    public static void main(String args[]){
        Mahasiswa4 s1 = new Mahasiswa4(123456, "Lutfi");
        Mahasiswa4 s2 = new Mahasiswa4(123457, "Rahma");
        
        s1.tampilkanInfo();
        s2.tampilkanInfo();
    }   
}
