/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class Gedung berisikan ruang (Penerapan composition)
 */

public class Gedung {
    private List<Ruang> daftarRuang = new ArrayList<Ruang>();
    
    // constructor
    public Gedung(){
        Ruang ruang =new Ruang("Utama");
        daftarRuang.add(ruang);
    }
    
    // Untuk menambahkan ruang ke list
    public void addRuang(String namaRuang){
        Ruang ruang = new Ruang(namaRuang);
        daftarRuang.add(ruang);
    }
    
    // Untuk mendapatkan list daftarRuang
    public List<Ruang> getDaftarRuang(){
        return daftarRuang;
    }
}
