/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Main Class, Kantor. Menerapkan semua class yang telah dibuat
 *            (Class Orang, Pegawai, Gedung, Ruang, UnitKerja)
 */

public class Kantor {
    public static void main(String[] args){
        // Penerapan class Orang
        System.out.println("--Inisiasi Objek Orang--");
        Orang lutfi = new Orang();
        lutfi.setNama("Lutfi");
        
        Orang rahma = new Orang("Rahma");
        rahma.setTanggalLahir(new Date(1997, 8, 31));
        
        System.out.println("Ada orang:");
        System.out.println(lutfi.getNama()+" lahir pada "+lutfi.getTanggalLahir());
        System.out.println(rahma.getNama()+" lahir pada "+rahma.getTanggalLahir());
        System.out.println();
        
        // Penerapan inheritance di class Pegawai dengan parent class Orang
        System.out.println("--Penerapan Inheritance--");
        Pegawai tuti = new Pegawai("Tuti", new Date(1997, 8, 2), "6836492379", "STIS", "IT");
        
        System.out.println(tuti.getNama()+" lahir pada "+tuti.getTanggalLahir()+" NIP: "+tuti.getNIP()+" kantor: "+tuti.getNamaKantor()+" bagian: "+tuti.getUnitKerja());
        
        Pegawai oui = new Pegawai();
        oui.setNama("Oui");
        oui.setTanggalLahir(new Date(2003, 02, 12));
        oui.setNIP("222112197");
        oui.setNamaKantor("Polstat STIS");
        oui.setUnitKerja("Komputasi Statistik");
        
        System.out.println(oui.getNama()+" lahir pada "+oui.getTanggalLahir()+" NIP: "+oui.getNIP()+" kantor: "+oui.getNamaKantor()+" bagian: "+oui.getUnitKerja());
        System.out.println();
        
        // Penerapan polimorphism
        System.out.println("--Penerapan Polimorphism--");
        System.out.println("Gaji Orang Rahma: "+rahma.getGaji());
        System.out.println("Gaji Orang Tuti: "+tuti.getGaji());
        System.out.println();
        
        // Penerapan aggregation
        System.out.println("--Penerapan Aggregation--");
        List<Pegawai> daftarPegawai = new ArrayList<Pegawai>();
        daftarPegawai.add(tuti);
        UnitKerja Umum = new UnitKerja("Umum", daftarPegawai);
        System.out.println("Unit Kerja: "+Umum.getNama());
        System.out.println();
        
        // Penerapan composition
        System.out.println("--Penerapan Composition--");
        Gedung STIS = new Gedung();
        STIS.addRuang("Lobi");
        STIS.addRuang("Bagian Umum");
        STIS.addRuang("Kepala Kantor");
        
        List<Ruang> ruangan = STIS.getDaftarRuang();
        for (Ruang ruang : ruangan){
            System.out.println("Ruang: "+ruang.getNamaRuang());
        }
    }
}
