/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;

import java.util.Date;
/**
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class Orang berisikan data nama dan tanggalLahir, disertai constructor
 *            dan beberapa method untuk mengubah nama, tanggalLahir, serta
 *            mendapatkan nama, tanggalLahir, dan gaji
 */

public class Orang {
    private String nama;
    private Date tanggalLahir;
    
    // Constructor tanpaparameter
    public Orang(){
        
    }
    
    // Constructor dengan parameter nama
    public Orang(String nama){
        this.nama = nama;
    }
    
    // Constructor dengan parameter nama dan tanggalLahir
    public Orang(String nama, Date tanggalLahir){
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }
    
    // Tiga constructor diatas merupakan penerapan overload constructor
    
    // Untuk mengubah nilai nama
    public void setNama(String nama){
        this.nama = nama;
    }
    
    // Untuk mendapatkan nilai nama
    public String getNama(){
        return nama;
    }
    
    // Untuk mengubah nilai tanggalLahir
    public void setTanggalLahir(Date tanggalLahir){
        this.tanggalLahir = tanggalLahir;
    }
    
    // Untuk mendapatkan nilai tanggalLahir
    public Date getTanggalLahir(){
        return tanggalLahir;
    }
    
    // Untuk mendapatkan nilai gaji
    public String getGaji(){
        return ("Tidak ada");
    }    
}
