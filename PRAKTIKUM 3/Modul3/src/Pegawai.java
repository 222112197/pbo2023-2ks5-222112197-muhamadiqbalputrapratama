/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;

import java.util.Date;

/**
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class Pegawai, merupakan turunan dari kelas Orang (Penerapan inheritance)
 *            berisikan variabel dalam class orang ditambah variabel NIP, namaKantor,
 *            dan unitKeja. Disertai beberapa method untuk mengubah dan mendapatkan nilai 
 *            variabel tersebut.
 */
public class Pegawai extends Orang{
    private String NIP;
    private String namaKantor;
    private String unitKerja;
    
    // Constructor tanpa parameter
    public Pegawai(){
    
    }
    
    // Constructor dengan tiga parameter
    public Pegawai(String NIP, String namaKantor, String unitKerja){
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    // Constructor dengan lima parameter
    public Pegawai(String nama, Date tanggalLahir, String NIP, String namaKantor, String unitKerja){
        super(nama, tanggalLahir); // Menurunkan nilai variabel nama dan tanggalLahir dari class Orang
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    // Tiga constructor diatas merupakan penerapan overload constructor
    
    // Untuk mendapatkan nilai variabel NIP
    public String getNIP(){
        return NIP;
    }
    
    // Untuk mengubah nilai variabel NIP
    public void setNIP(String NIP){
        this.NIP = NIP;
    }
    
    // Untuk mendapatkan nilai variabel namaKantor
    public String getNamaKantor(){
        return namaKantor;
    }
    
    // Untuk mengubah nilai variabel namaKantor
    public void setNamaKantor(String namaKantor){
        this.namaKantor = namaKantor;
    }
    
    // Untuk mendapatkan nilai variabel unitKerja
    public String getUnitKerja(){
        return unitKerja;
    }
    
    // Untuk mengubah nilai variabel unitKerja
    public void setUnitKerja(String unitKerja){
        this.unitKerja = unitKerja;
    }
    
    // Penerapan overriding
    @Override
    public String getGaji(){
       return ("7 juta");
    }
}

