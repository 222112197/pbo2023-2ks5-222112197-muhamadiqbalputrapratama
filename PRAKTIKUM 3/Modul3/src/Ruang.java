/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;

/**
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class Ruang, berisikan variabel nama ruang. Dibuat untuk penerapan 
 *            composition
 */

public class Ruang {
    private String namaRuang;
    
    // Constructor
    public Ruang(String namaRuang){
        this.namaRuang = namaRuang;
    }
    
    // Untuk mendapatkan nilai variabel namaRuang
    public String getNamaRuang(){
        return namaRuang;
    }
    
    // Untuk mengubah nilai variabel namaRuang
    public void setNamaRuang(String namaRuang){
        this.namaRuang = namaRuang;
    }
}
