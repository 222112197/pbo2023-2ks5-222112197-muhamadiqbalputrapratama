/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: 222112197_Muhamad Iqbal
 * Deskripsi: Class UnitKerja, dibuat untuk penerapan aggregation
 */
public class UnitKerja {
    private String nama;
    private List<Pegawai> daftarPegawai;
    
    // Constructor
    public UnitKerja(String nama, List<Pegawai> pegawais){
        this.nama = nama;
        this.daftarPegawai = pegawais;
    }
    
    // Untuk mendapatkan nilai variabel nama
    public String getNama(){
        return nama;
    }
    
    // Untuk mendapatkan list daftarPegawai
    public List<Pegawai> getDaftarPegawai(){
        return daftarPegawai;
    }
}
