/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

/**
 *
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Bagian ini merupakan interface yang bertipe public
 * 
 */
interface Dosen {
    public String getNIDN();
    public void setNIDN(String NIDN);
    public String getKelompokKeahlian();
    public void setKelompokKeahlian(String kelompokKeahlian);
}
