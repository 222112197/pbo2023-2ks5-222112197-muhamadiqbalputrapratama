/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

import java.util.Date;

/**
 *
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Bagian ini merupakan abstract class orang
 * 
 */
abstract class Orang {
    private String nama;
    private Date tanggalLahir;
    
    public Orang(){
    }
    
    public Orang(String nama){
        this.nama = nama;
    }
    
    public Orang(String nama, Date tanggalLahir){
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }
    
    abstract public String getAlamat();
    abstract public void setAlamat(String alamat);
    
    
}
