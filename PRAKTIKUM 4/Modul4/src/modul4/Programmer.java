/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

import java.util.Date;

/**
 *
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Class Programmer yang merupakan turunan dari Class Pegawai
 * 
 */

public class Programmer extends Pegawai{
    private String bahasaPemrograman;
    private String platform;
    
    public Programmer(String nama, Date tanggalLahir, String NIP, String kantor, String unitKerja, String bp, String platform) {
        super(nama, tanggalLahir, NIP, kantor, unitKerja);
        this.bahasaPemrograman = bp;
        this.platform = platform;
    }
    
    public void setBahasaaPemrograman(String bp){
        this.bahasaPemrograman = bp;
    }
    
    public String getBahasaPemrograman(){
        return bahasaPemrograman;
    }
    
    public void setPlatform(String platform){
        this.platform = platform;
    }
    
    public String getPlatform(){
        return platform;
    }
}
