/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

import java.util.Date;

/**
 * 
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Mainclass yang menerapkan semua class yang telah dibuat sebelumnya
 * 
 */

public class TestClass {
    public static void main(String[] args) {
    
 //     Orang o = new Orang(); --> error karena Orang merupakan abstract
        
        //Tidak Mengirimkan Parameter Karena ingin mecoba set parameter sendiri
        Pegawai lutfi = new Pegawai();
        lutfi.setAlamat("Otista 64 C");
        lutfi.setNIDN("12345678");
        lutfi.setKelompokKeahlian("Computer Science");
        System.out.println("Ada dosen Lutfi dengan NIDN " +lutfi.getNIDN()+" kelompok "+lutfi.getKelompokKeahlian()+"\nTinggal di "+lutfi.getAlamat());
        lutfi.setNamaKantor("STIS");
        lutfi.setUnitKerja("Back-end Developer");
        System.out.println("Bekerja di "+lutfi.getNamaKantor()+" pada bagian "+lutfi.getUnitKerja());
    
        //Test Kelas Programmer
        System.out.println("");
        Programmer Oui = new Programmer("Oui", new Date(2002, 07, 19), "222112197", "Polstat STIS", "IT", "Java", "NetBeans");
        System.out.println("Deskripsi Programmer: Oui ");
        System.out.println("Bahasa Pemrograman: "+Oui.getBahasaPemrograman());
        System.out.println("Platform yang digunakan: "+Oui.getPlatform());
        
    }
}
