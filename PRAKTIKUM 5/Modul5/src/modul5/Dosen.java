/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modul5;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Interface class Dosen yang akan implements di kelas lainnya
 */

public interface Dosen {
    static final String NIDN = "12345";
    static final String kelompokKeahlian = "Statistik";
    public String getNIDN();
    public void setNIDN(String NIDN);
    public String getKeahlian();
    public String getPekerjaan();
    public void setKeahlian(String keahlian);
}

