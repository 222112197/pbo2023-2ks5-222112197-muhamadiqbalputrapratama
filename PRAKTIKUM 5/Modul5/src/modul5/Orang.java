/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul5;

import java.time.LocalDate;


/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Abstract class Orang yang akan extends di kelas lainnya
 */

public abstract class Orang {
    private String nama;
    private LocalDate tanggalLahir;
    
    public Orang(String nama, LocalDate tanggalLahir){
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }
    
    public String getNama(){
        return nama;
    }
    
    public void setNama(String nama){
        this.nama = nama;
    }
    
    public String getNamaPanggilan(){
        return nama.substring(0, 2);
    }
}
