/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul5;

import java.time.LocalDate;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Class Pegawai yang mengextends kelas Orang dan mengimplements 
 *            class Dosen
 */

public class Pegawai extends Orang implements Dosen {
    private String NIP;
    private String kantor;
    private String unitKerja;
    private String NIDN;
    private String kelompokKeahlian;

    public Pegawai(String nama, LocalDate tanggalLahir) {
        super(nama, tanggalLahir);
    }
    
    public String getNIP(){
        return NIP;
    }
    
    public String getKantor(){
        return kantor;
    }
    
    public void setNIP(String NIP){
        this.NIP = NIP;
    }

    @Override
    public String getNIDN() {
         return NIDN;
    }

    @Override
    public void setNIDN(String NIDN) {
        this.NIDN = NIDN;
    }

    @Override
    public String getKeahlian() {
        return kelompokKeahlian;
    }

    @Override
    public String getPekerjaan() {
        return "Mengajar";
    }

    @Override
    public void setKeahlian(String keahlian) {
        this.kelompokKeahlian = keahlian;
    }
}
