/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul5;

import java.time.LocalDate;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Class Programer yang mengextends class Pegawai
 */

public class Programmer extends Pegawai{
    private String bahasaPemrograman;
    private String platform;
    
    public Programmer(String nama, LocalDate tanggalLahir) {
        super(nama, tanggalLahir);
    }

    public String getBahasa(){
        return bahasaPemrograman;
    }    

    public void setBahasa(String bahasa){
        this.bahasaPemrograman = bahasa;
    }

    public String getPlatform(){
        return platform;
    }
    
    public String getPekerjaan(){
        return "Coding all along day";
    }
    
    public void setPlatform(String platform){
        this.platform = platform;
    }
}