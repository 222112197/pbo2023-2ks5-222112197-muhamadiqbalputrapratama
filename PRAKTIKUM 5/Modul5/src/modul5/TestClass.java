/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package modul5;

import java.time.LocalDate;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: TestClass yang menjalankan class-class yang telah dibuat sebelumnya
 */

public class TestClass {
    //@throws java.lang.Exception
    public static void main(String[] args){
        //Inisisasi pegawai Rahma
        LocalDate lahir = LocalDate.of(2015,8,31);
        Pegawai rahma=new Pegawai("rahma", lahir);
        rahma.setNIP("123456");
        
        //Inisisasi programer Lutfi
        Programmer lutfi=new Programmer("lutfi", LocalDate.of(2010, 2, 20));
        lutfi.setNIP("256289");
        
        System.out.println("Nama: "+rahma.getNama()+", NIP: "+rahma.getNIP());
        System.out.println("Nama: "+lutfi.getNama()+", NIP: "+lutfi.getNIP());
    }   
}
