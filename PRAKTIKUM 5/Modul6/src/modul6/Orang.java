/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul6;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Abstract class Orang yang akan extends di kelas lainnya
 */

abstract class Orang {
    //properties
    private String nama;
    private Calendar tanggalLahir;
    
    //overload constructor
    public Orang(String nama){
        this.nama = nama;
    }
    
    //overload constructor
    public Orang(String nama, Calendar tanggalLahir){
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }
    
    //getter nama
    public String getNama(){
        return nama;
    }
    
    //setter nama
    public void setNama(String nama){
        this.nama = nama;
    }
    
    //getter nama panggilan
    public String getNamaPanggilan(){
        return nama.substring(0, 3);
    }
    
    //setter tanggal lahir
    public void setTanggalLahir(GregorianCalendar tanggalLahir){
        this.tanggalLahir = tanggalLahir;
    }
    
    //getter tanggal lahir
    public String getTanggalLahir(){
    try{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
        return sdf.format(tanggalLahir.getTime()).toString();
    } catch (Exception e){
        return "Belum Tersedia.";
    } finally{
        System.out.println("error tertangani");
    }
    }
    
    //validasi tanggal lahir
    void validatetanggalLahir() throws Exception{
        if(tanggalLahir.get(Calendar.YEAR)<2000){
            throw new Exception("Dibawah umur");
        }
        else {
            System.out.println("cukup umur");
        }
    }
}


