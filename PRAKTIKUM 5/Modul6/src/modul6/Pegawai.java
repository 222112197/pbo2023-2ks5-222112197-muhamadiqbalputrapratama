/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul6;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Class Pegawai yang mengextends kelas Orang dan mengimplements 
 *            class Dosen
 */

public class Pegawai extends Orang implements Dosen{
    //properties
    private String NIP;
    private String kantor;
    private String unitKerja;
    private String NIDN;
    private String keahlian;
    
    public Pegawai(String nama){
        super(nama);
    }
    
    //overload constructor
    public Pegawai(String nama, Calendar tanggalLahir, String NIP, String kantor, String unitKerja){
        super(nama,tanggalLahir);
        this.NIP = NIP;
        this.kantor = kantor;
        this.unitKerja = unitKerja;
    }
    
    //getter NIP
    public String getNIP(){
        return NIP;
    }
    
    //setter NIP
    public void setNIP(String NIP){
        this.NIP = NIP;
    }
    
    //getter kantor
    public String getKantor(){
        return kantor;
    }
    
    //stter kantor
    public void setKantor(String kantor){
        this.kantor = kantor;
    }
    
    //getter unit kerja
    public String getUnitKerja(){
        return unitKerja;
    }
    
    //setter unit kerja
    public void setUnitKerja(String unitKerja){
        this.unitKerja = unitKerja;
    }
    
    //getter NIDN
    @Override
    public String getNIDN(){
           return NIDN;
    }
    
    //setter NIDN
    @Override
    public void setNIDN(String NIDN){
        this.NIDN=NIDN;
    }
    
    //getter kelompok keahlian
    @Override
    public String getKeahlian(){
        return keahlian;
    }
    
    //getter pekerjaan
    @Override
    public String getPekerjaan(){
        return "mengajar";
    }
    
    //setter kelompok keahlian
    @Override
    public void setKeahlian(String keahlian){
        this.keahlian=keahlian;
    }
}

