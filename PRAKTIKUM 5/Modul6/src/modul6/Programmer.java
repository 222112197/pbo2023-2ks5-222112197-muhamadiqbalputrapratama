/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul6;

/**
 *
 * @author hp
 */
import java.util.Calendar;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: Class Programer yang mengextends class Pegawai
 */

public class Programmer extends Pegawai{
    //properties
    private String bahasaPemrograman;
    private String platform;

    //overload constructor
    public Programmer(String nama, Calendar tanggalLahir, String NIP, String kantor, String unitKerja, String bahasaPemrograman, String platform) {
        super(nama, tanggalLahir, NIP, kantor, unitKerja);
        this.bahasaPemrograman = bahasaPemrograman;
        this.platform = platform;
    }
    
    //getter bahasa pemrograman
    public String getBahasa(){
        return bahasaPemrograman;
    }
    
    //setter bahasa pemrograman
    public void setBahasa(String bahasaPemrograman){
        this.bahasaPemrograman = bahasaPemrograman;
    }
    
    //getter platform
    public String getPlatform(){
        return platform;
    }
    
    //getter pekerjaan
    @Override
    public String getPekerjaan(){
        return "Coding all along day";
    }
    
    //setter platform
    public void setPlatform(String platform){
        this.platform = platform;
    }
    
}

