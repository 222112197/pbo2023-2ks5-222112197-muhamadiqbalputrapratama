/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package modul6;

import java.util.GregorianCalendar;

/**
 * Author: 222112197_Muhamad Iqbal Putra Pratama
 * Deskripsi: TestClass yang menjalankan class-class yang telah dibuat sebelumnya
 */

public class TestClass {
    //@throws java.lang.Exception
    public static void main(String[] args) throws Exception{
        Pegawai rahma=new Pegawai("rahma");
        rahma.setTanggalLahir(new GregorianCalendar(2015,8,31));
        Pegawai lutfi=new Pegawai("lutfi");
        System.out.println("Ada orang : ");
        System.out.println(lutfi.getNama()+" lahir pada "+lutfi.getTanggalLahir());
        rahma.validatetanggalLahir();
        System.out.println(rahma.getNama()+" lahir pada "+rahma.getTanggalLahir());
        
    }   
}
